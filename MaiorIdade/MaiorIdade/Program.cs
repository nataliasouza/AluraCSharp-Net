﻿using System;

namespace MaiorIdade
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*** Executando projeto 7 | Condicionais ***\n");

            int idade;
            string acompanhante;

            Console.Write("Digite a sua idade: ");
            idade = int.Parse(Console.ReadLine());

            if (idade >= 18)
            {
                Console.WriteLine("\nÉ maior de idade!\nTem: " + idade + " anos! Pode entrar!");
            } else
            {
                Console.Write("\nEstá acompanhado(a) de um responsável e maior de idade? ");
                Console.Write("\nDigite sim ou nao: ");
                acompanhante = Console.ReadLine().ToLower();

                if (acompanhante == "sim")
                {
                    Console.WriteLine("\nEstá acompanhado de um responsável maior de Idade! Pode entrar!");
                }
                else
                {
                    Console.WriteLine("\nÉ menor de 18 anos! \nTem: " + idade + " anos e não está acompanhado! Não Pode entrar!");
                }
            } 
        }
    }
}
