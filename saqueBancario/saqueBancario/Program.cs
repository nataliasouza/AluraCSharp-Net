﻿using System;
using System.Globalization;
using System.Threading;

namespace saqueBancario
{
    class Program
    {
        static void Main(string[] args)
        {
            double deposito, saldoConta, saque, restoSaque;

            Console.Write("Digite o valor para fazer o depósito: R$");
            deposito = double.Parse(Console.ReadLine());

            saldoConta = deposito;

            Console.WriteLine("\n====== SALDO DA CONTA ======");
            Console.WriteLine("\nSeu saldo atual: R$"+saldoConta.ToString("F2", CultureInfo.InvariantCulture).Replace(".", ","));
            Console.WriteLine("\n===========================");

            Console.Write("\nDigite o valor do saque: ");
            saque = double.Parse(Console.ReadLine());

            if (saldoConta >= saque)
            {
                restoSaque = saldoConta - saque;
                Console.WriteLine("SAQUE REALIZADO COM SUCESSO!!!");
                Thread.Sleep(2000);

                Console.Clear();
                Console.WriteLine("\n====== SALDO DA CONTA ======");
                Console.WriteLine("\nSeu saldo atual: "+ restoSaque.ToString("F2", CultureInfo.InvariantCulture).Replace(".", ","));
                Console.WriteLine("\n===========================");
            }
            else
                Console.WriteLine("\nSaldo Indisponível");
            
        }
    }
}
