﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fatorial
{
    class Program
    {
        static void Main(string[] args)
        {
            int fatorial = 1;
            for(int num = 1; num <= 10; num++)
            {
                fatorial *= num;
                Console.WriteLine("Fatorial de " + num + " = " + fatorial);
            }
            Console.ReadKey();
        }
    }
}
