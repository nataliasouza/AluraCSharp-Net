﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace _11_CalculaPoupanca2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Executando o projeto 11");

            double valorInvestido = 1000;

            for(int contadorMes = 1; contadorMes <=12; contadorMes++)
            {
                valorInvestido *= 1.0036;
                Console.WriteLine("O valor do "+ contadorMes + "º mês de investimento: R$"+valorInvestido.ToString("F2"));
            }
            Console.ReadLine();
        }
    }
}
