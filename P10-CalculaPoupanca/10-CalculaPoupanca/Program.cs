﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace _10_CalculaPoupanca
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Executando projeto 10 - Calcula Poupança");

            double valorInvestido = 1000;
            int contadorMes = 1;

            while(contadorMes <= 12)
            {
                valorInvestido = valorInvestido + valorInvestido * 0.0036;
                Console.WriteLine("No "+contadorMes+"º mês, você terá: " + valorInvestido.ToString("F2", CultureInfo.InvariantCulture).Replace(".",","));

                //contadorMes = contadorMes + 1;
                //contadorMes += 1;
                contadorMes++;
            }
           
            Console.ReadLine();
        }
    }
}
