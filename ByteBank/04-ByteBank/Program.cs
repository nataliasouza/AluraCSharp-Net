﻿using System;

namespace _04_ByteBank
{
    class Program
    {
        static void Main(string[] args)
        {
            ContaCorrente contaDoMike = new ContaCorrente();
            contaDoMike.titular = "Mike";

            Console.WriteLine("Saldo Atual da Conta do Mike: " + contaDoMike.saldo);
            bool resultadoSaque = contaDoMike.Sacar(450);
            Console.WriteLine("Resultado do Saque: " + resultadoSaque);

            Console.WriteLine("\n=====SAQUE=====");
            contaDoMike.Sacar(50);
            Console.WriteLine("Saque realizado! \nSaldo Atual: " + contaDoMike.saldo);
            
            Console.WriteLine("\n=====DEPOSITO=====");
            contaDoMike.Deposito(500);
            Console.WriteLine("Depósito Realizado com Sucesso! \nSaldo Atual: " + contaDoMike.saldo);

            ContaCorrente contaDaMalu = new ContaCorrente();
            contaDaMalu.titular = "Malu";

            Console.WriteLine("\n=====SALDO DAS CONTAS=====");
            Console.WriteLine("Saldo da Conta do Mike: " + contaDoMike.saldo);
            Console.WriteLine("Saldo da Conta do Malu: " + contaDaMalu.saldo);

            bool resultadoTransferencia = contaDoMike.Transferir(200, contaDaMalu);

            Console.WriteLine("\n=====TRANSFERÊNCIA=====");
            Console.WriteLine("Saldo Atual da Conta do Mike: " + contaDoMike.saldo);
            Console.WriteLine("Saldo Atual da Conta do Malu: " + contaDaMalu.saldo);
            Console.WriteLine("Resultado da Transferência: " + resultadoTransferencia);

            Console.ReadLine();
        }
    }
}
