﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_ByteBank
{
    class Program
    {
        static void Main(string[] args)
        {
            ContaCorrente contaDaNatalia = new ContaCorrente();
            contaDaNatalia.titular = "Natália";
            contaDaNatalia.agencia = 863;
            contaDaNatalia.numero = 863452;

            ContaCorrente contaDaNataliaSouza = new ContaCorrente();
            contaDaNataliaSouza.titular = "Natalia Souza";
            contaDaNataliaSouza.agencia = 863;
            contaDaNataliaSouza.numero = 989123;

            Console.WriteLine("Igualdade de tipo de referência: " + (contaDaNatalia == contaDaNataliaSouza));

            int idade = 27;
            int idadeMaisUmaVez = 27;

            Console.WriteLine("Igualdade de tipo de valor: " + (idade == idadeMaisUmaVez));

            contaDaNatalia = contaDaNataliaSouza;
            Console.WriteLine(contaDaNatalia == contaDaNataliaSouza);

            contaDaNatalia.saldo = 300;
            Console.WriteLine(contaDaNatalia.saldo);
            Console.WriteLine(contaDaNataliaSouza.saldo);

            Console.ReadLine();
        }
    }
}
