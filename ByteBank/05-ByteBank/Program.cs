﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_ByteBank
{
    class Program
    {
        static void Main(string[] args)
        {
            Cliente dadosDoTitular = new Cliente();

            dadosDoTitular.nome = "Natalia";
            dadosDoTitular.profissao = "Desenvolvedora C#";
            dadosDoTitular.cpf = "978.654.432-01";

            ContaCorrente conta = new ContaCorrente();

            conta.titular = dadosDoTitular;
            conta.agencia = 197;
            conta.numero = 3130090;
            conta.saldo = 500;

            Console.WriteLine(dadosDoTitular.nome); 
            Console.WriteLine(conta.titular.nome);

            Console.ReadKey();
        }
    }
}
