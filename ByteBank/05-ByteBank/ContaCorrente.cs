﻿namespace _05_ByteBank
{
    public class ContaCorrente
    {
        public Cliente titular;
        public int agencia;
        public int numero;
        public double saldo = 200;

        public bool Sacar(double valor)
        {
            if (this.saldo < valor)
            {
                return false;
            }
            
            this.saldo -= valor;
            return true;   
        }

        public void Deposito(double valorDeposito)
        {
            this.saldo += valorDeposito;
        }

        public bool Transferir(double valor, ContaCorrente contaDestino)
        {
            if(this.saldo < valor)
            {
                return false;
            }
            else
            
            this.saldo -= valor;
            contaDestino.Deposito(valor);
            return true;
            
        }
    }
}
