﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ByteBank
{
    class Program
    {
        static void Main(string[] args)
        {
            ContaConrrente contaDaGabriela = new ContaConrrente();
            contaDaGabriela.titular = "Gabriela";
            contaDaGabriela.agencia = 863;
            contaDaGabriela.numero = 863423;
            contaDaGabriela.saldo = 100;

            Console.WriteLine(contaDaGabriela.titular);
            Console.WriteLine("Agencia: " + contaDaGabriela.agencia);
            Console.WriteLine("Número da Conta: " + contaDaGabriela.numero);
            Console.Write("Saldo: R$" + contaDaGabriela.saldo);

            contaDaGabriela.saldo += 200;
            Console.Write("Novo Saldo: R$" + contaDaGabriela.saldo);

            Console.ReadLine();
        }
    }
}
